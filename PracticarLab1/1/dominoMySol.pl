subconjunto([],[]).
subconjunto([X|L1],[X|L2]) :- subconjunto(L1,L2).
subconjunto(L1,[_|L2]) :- subconjunto(L1,L2).

subsetOfSize(0,_,[]):-!.
subsetOfSize(N,[X|L],[X|S]):- N1 is N-1, length(L,Leng), Leng>=N1, subsetOfSize(N1,L,S).
subsetOfSize(N,[_|L],   S ):-            length(L,Leng), Leng>=N,  subsetOfSize(N,L,S).

chain([],[]).
chain([[X,Y]], [[X,Y]]).
chain([[X,Y]], [[Y,X]]).
chain([[X,Y]|L], [[X,Y]|R]) :- chain(L,R), R = [ [Y,_] | _ ].
chain([[X,Y]|L], [[Y,X]|R]) :- chain(L,R), R = [ [X,_] | _ ].


%all_chains(L):- length(L,P), between(0,P,N), subsetOfSize(N,L,I), permutation(I,J), chain(J,R), write(R), nl, fail.
%all_chains(_).

all_chains(L):- subconjunto(I,L), permutation(I,J), chain(J,R), write(R), nl, fail.
all_chains(_).
