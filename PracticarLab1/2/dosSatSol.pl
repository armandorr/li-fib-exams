
pert_r(X,[X|L],L).
pert_r(X,[Y|L],[Y|R]):- pert_r(X,L,R).

path(_,N,N):- !.
path(L,N1,N2):- pert_r([N1,Y],L,R), path(R,Y,N2),!.

negate(\+X,X):-!.
negate(X,\+X).

sat(N,S):-
   findall([NX,Y],(member([X,Y],S),negate(X,NX)),G1),
   findall([NY,X],(member([X,Y],S),negate(Y,NY)),G2),  append(G1,G2,G),
   \+badCycle(N,G).

badCycle(N,G):- between(1,N,X), negate(X,Y), path(G,X,Y), path(G,Y,X).
