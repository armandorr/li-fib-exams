%% COVID: ompliu:
%la primera fila    és la de la pisarra
%la primera columna és la de la esquerra, mirant des de la pisarra
filaColumnaAula(5,1,c6S306). % fila columna aula


symbolicOutput(0).  % set to 1 to see symbolic output 
                    % only; 0 otherwise.

% We have a one-day event where 9 different presentations are given, in three slots:
% morning, afternoon and evening.  There are 24 attendants. Each one of them requests 3 of
% the 9 presentations (s)he wants to attend, as indicated below; for example, the first
% attendant wants to go to presentations 1, 2 and 6, it does not matter in which order.  To
% satisfy all requests, we could of course plan all 9 presentations in all three slots (27
% presentations in total), but we want to have at most 5 presentations in the evening, at
% most 4 presentations in the morning and at most 4 in the afternoon. Complete this program
% for planning this using the picosat SAT solver. The output should be a correct planning like:
%
%      Presentations: 
%        Morning:   [1,8,9,5]
%        Afternoon: [6,3,4,5]
%        Evening:   [6,2,7,8,5]
%
% Check that with this plan indeed everybody can attend their presentations in some order.

requests([	[1,2,6],[1,6,7],[2,3,8],[6,7,9],[6,8,9],[1,2,4],[3,5,6],[3,5,7],
		[5,6,8],[1,6,8],[4,7,9],[4,6,9],[1,4,6],[3,6,9],[2,3,5],[1,4,5],
		[1,6,7],[6,7,8],[1,2,4],[1,5,7],[2,5,6],[2,3,5],[5,7,9],[1,6,8]
	]).

%%%%%% Some helpful definitions to make the code cleaner:
slot(S):-             between(1,3,S).
presentation(P):-     between(1,9,P).
request([P1,P2,P3]):- requests(L), member([P1,P2,P3],L).

%%%%%%  1. SAT Variables:

% p(P,S) means "presentation P is held in slot S"    P in 1..9, S in 1..3
satVariable( p(P,S) ):- presentation(P), slot(S).

% order(P1,P2,P3) means "presentation P1 is in slot 1  AND  P2 in slot 2  AND  P3 is in slot 3".
satVariable( order(P1,P2,P3) ):- request(R), permutation(R,[P1,P2,P3]).


%%%%%%  2. Clause generation:

writeClauses:- 
    orderImpliesP,              % relationship between the SAT variables "order" and "p"
    eachRequestAtLeastOneOrder,
    most5evening,
    most4morn_aft,
    true,!.
writeClauses:- told, nl, write('writeClauses failed!'), nl,nl, halt.

%at most 5 at evening
most5evening:- findall(p(P3,3),presentation(P3),Lits), atMost(5,Lits).
most5evening.

%at most 4 at afternoon and at most 4 morning
most4morn_aft:- slot(S), S\=3, findall(p(P,S),presentation(P),Lits), atMost(4,Lits), fail.
most4morn_aft.

% order(P1,P2,P3) -> P1 & P2 & P3  ==>
% (-order(P1,P2,P3) or p(P1,1)) & (-order(P1,P2,P3) or p(P2,2)) & (-order(P1,P2,P3) or p(P3,3))
orderImpliesP:- 
    request(R), permutation(R,[P1,P2,P3]),
    writeClause([-order(P1,P2,P3),p(P1,1)]),
    writeClause([-order(P1,P2,P3),p(P2,2)]),
    writeClause([-order(P1,P2,P3),p(P3,3)]),
    fail.
orderImpliesP.

% at least one order for every request to make everyone see the desired presentations
eachRequestAtLeastOneOrder:- request(R), findall(order(P1,P2,P3),permutation(R,[P1,P2,P3]),Lits), writeClause(Lits), fail.
eachRequestAtLeastOneOrder.


%%%%%%  3. DisplaySol: show the solution. Here M contains the literals that are true in the model:

%displaySol(M):- nl, write(M), nl, nl, fail.
displaySol(M):- 
    findall(P, member(p(P,1),M), Morning   ),
    findall(P, member(p(P,2),M), Afternoon ),
    findall(P, member(p(P,3),M), Evening   ),
    nl, write(' Presentations: '), nl,
    write('   Morning:   '), write( Morning   ), nl,
    write('   Afternoon: '), write( Afternoon ), nl,
    write('   Evening:   '), write( Evening   ), nl, halt.


%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Everything below is given as a standard library, reusable for solving 
%    with SAT many different problems.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Express that Var is equivalent to the disjunction of Lits:
expressOr( Var, Lits ):- symbolicOutput(1), write( Var ), write(' <--> or('), write(Lits), write(')'), nl, !. 
expressOr( Var, Lits ):- member(Lit,Lits), negate(Lit,NLit), writeClause([ NLit, Var ]), fail.
expressOr( Var, Lits ):- negate(Var,NVar), writeClause([ NVar | Lits ]),!.

%% expressOr(a,[x,y]) genera 3 clausulas (como en la Transformación de Tseitin):
%% a == x v y
%% x -> a       -x v a
%% y -> a       -y v a
%% a -> x v y   -a v x v y

% Express that Var is equivalent to the conjunction of Lits:
expressAnd( Var, Lits) :- symbolicOutput(1), write( Var ), write(' <--> and('), write(Lits), write(')'), nl, !. 
expressAnd( Var, Lits):- member(Lit,Lits), negate(Var,NVar), writeClause([ NVar, Lit ]), fail.
expressAnd( Var, Lits):- findall(NLit, (member(Lit,Lits), negate(Lit,NLit)), NLits), writeClause([ Var | NLits]), !.


%%%%%% Cardinality constraints on arbitrary sets of literals Lits:

exactly(K,Lits):- symbolicOutput(1), write( exactly(K,Lits) ), nl, !.
exactly(K,Lits):- atLeast(K,Lits), atMost(K,Lits),!.

atMost(K,Lits):- symbolicOutput(1), write( atMost(K,Lits) ), nl, !.
atMost(K,Lits):-   % l1+...+ln <= k:  in all subsets of size k+1, at least one is false:
	negateAll(Lits,NLits),
	K1 is K+1,    subsetOfSize(K1,NLits,Clause), writeClause(Clause),fail.
atMost(_,_).

atLeast(K,Lits):- symbolicOutput(1), write( atLeast(K,Lits) ), nl, !.
atLeast(K,Lits):-  % l1+...+ln >= k: in all subsets of size n-k+1, at least one is true:
	length(Lits,N),
	K1 is N-K+1,  subsetOfSize(K1, Lits,Clause), writeClause(Clause),fail.
atLeast(_,_).

negateAll( [], [] ).
negateAll( [Lit|Lits], [NLit|NLits] ):- negate(Lit,NLit), negateAll( Lits, NLits ),!.

negate( -Var,  Var):-!.
negate(  Var, -Var):-!.

subsetOfSize(0,_,[]):-!.
subsetOfSize(N,[X|L],[X|S]):- N1 is N-1, length(L,Leng), Leng>=N1, subsetOfSize(N1,L,S).
subsetOfSize(N,[_|L],   S ):-            length(L,Leng), Leng>=N,  subsetOfSize( N,L,S).


%%%%%% main:

main:-  nl,nl,nl,filaColumnaAula(F,C,A), 
	write('Estic a la fila '), write(F),
	write(' columna '),        write(C),
	write(' de l''aula '),     write(A), write('.'), nl,nl,nl, fail.
main:-  symbolicOutput(1), !, writeClauses, halt.   % print the clauses in symbolic form and halt
main:-  initClauseGeneration,
	tell(clauses), writeClauses, told,          % generate the (numeric) SAT clauses and call the solver
	tell(header),  writeHeader,  told,
	numVars(N), numClauses(C),
	write('Generated '), write(C), write(' clauses over '), write(N), write(' variables. '),nl,
	shell('cat header clauses > infile.cnf',_),
	write('Calling solver....'), nl,
	shell('picosat -v -o model infile.cnf', Result),  % if sat: Result=10; if unsat: Result=20.
	treatResult(Result),!.

treatResult(20):- write('Unsatisfiable'), nl, halt.
treatResult(10):- write('Solution found: '), nl, see(model), symbolicModel(M), seen, displaySol(M), nl,nl,halt.
treatResult( _):- write('cnf input error. Wrote anything strange in your cnf?'), nl,nl, halt.
    

initClauseGeneration:-  %initialize all info about variables and clauses:
	retractall(numClauses(   _)),
	retractall(numVars(      _)),
	retractall(varNumber(_,_,_)),
	assert(numClauses( 0 )),
	assert(numVars(    0 )),     !.

writeClause([]):- symbolicOutput(1),!, nl.
writeClause([]):- countClause, write(0), nl.
writeClause([Lit|C]):- w(Lit), writeClause(C),!.
w(-Var):- symbolicOutput(1), satVariable(Var), write(-Var), write(' '),!. 
w( Var):- symbolicOutput(1), satVariable(Var), write( Var), write(' '),!. 
w(-Var):- satVariable(Var),  var2num(Var,N),   write(-), write(N), write(' '),!.
w( Var):- satVariable(Var),  var2num(Var,N),             write(N), write(' '),!.
w( Lit):- told, write('ERROR: generating clause with undeclared variable in literal '), write(Lit), nl,nl, halt.


% given the symbolic variable V, find its variable number N in the SAT solver:
:-dynamic(varNumber / 3).
var2num(V,N):- hash_term(V,Key), existsOrCreate(V,Key,N),!.
existsOrCreate(V,Key,N):- varNumber(Key,V,N),!.                            % V already existed with num N
existsOrCreate(V,Key,N):- newVarNumber(N), assert(varNumber(Key,V,N)), !.  % otherwise, introduce new N for V

writeHeader:- numVars(N),numClauses(C), write('p cnf '),write(N), write(' '),write(C),nl.

countClause:-     retract( numClauses(N0) ), N is N0+1, assert( numClauses(N) ),!.
newVarNumber(N):- retract( numVars(   N0) ), N is N0+1, assert(    numVars(N) ),!.

% Getting the symbolic model M from the output file:
symbolicModel(M):- get_code(Char), readWord(Char,W), symbolicModel(M1), addIfPositiveInt(W,M1,M),!.
symbolicModel([]).
addIfPositiveInt(W,L,[Var|L]):- W = [C|_], between(48,57,C), number_codes(N,W), N>0, varNumber(_,Var,N),!.
addIfPositiveInt(_,L,L).
readWord( 99,W):- repeat, get_code(Ch), member(Ch,[-1,10]), !, get_code(Ch1), readWord(Ch1,W),!. % skip line starting w/ c
readWord(115,W):- repeat, get_code(Ch), member(Ch,[-1,10]), !, get_code(Ch1), readWord(Ch1,W),!. % skip line starting w/ s
readWord(-1,_):-!, fail. %end of file
readWord(C,[]):- member(C,[10,32]), !. % newline or white space marks end of word
readWord(Char,[Char|W]):- get_code(Char1), readWord(Char1,W), !.
%========================================================================================
