%% COVID: ompliu:
%la primera fila    és la de la pisarra
%la primera columna és la de la esquerra, mirant des de la pisarra
filaColumnaAula(5,1,c6S306). % fila columna aula1

% We have a one-day event where 9 different presentations are given, in three slots:
% morning, afternoon and evening.  There are 24 attendants. Each one of them requests 3 of
% the 9 presentations (s)he wants to attend, as indicated below; for example, the first
% attendant wants to go to presentations 1, 2 and 6, it does not matter in which order.
% To satisfy all requests, we could of course plan all 9 presentations in all three slots
% (27 presentations in total), but we want to do it with less than 27, minimizing the
% total number of presentations. Complete the following Prolog program to do this.

requests([	[1,2,6],[1,6,7],[2,3,8],[6,7,9],[6,8,9],[1,2,4],[3,5,6],[3,5,7],
		[5,6,8],[1,6,8],[4,7,9],[4,6,9],[1,4,6],[3,6,9],[2,3,5],[1,4,5],
		[1,6,7],[6,7,8],[1,2,4],[1,5,7],[2,5,6],[2,3,5],[5,7,9],[1,6,8]
	]).

% The output should be something like:
%     13 presentations are needed: 
%       Morning:  [7,6,3,1]
%       Afternoon:[5,9,6,2]
%       Evening:  [5,4,8,7,6]
% Check that with this plan indeed everybody can attend their presentations in some order.

main:-  nl,nl,nl,filaColumnaAula(F,C,A), 
	write('Estic a la fila '), write(F),
	write(' columna '),        write(C),
	write(' de l''aula '),     write(A), write('.'), nl,nl,nl, fail.

main:- requests(Requests), 
       between(1,1000,N),  % first try with N=1, then N=2, etc, until it works
       distribute( N, Requests, [ [], [], [] ],  [ Morning, Afternoon, Evening ] ), 
       nl, write(N), write(' presentations are needed: '), nl,
       write('  Morning:   '), write( Morning   ), nl,
       write('  Afternoon: '), write( Afternoon ), nl,
       write('  Evening:   '), write( Evening   ), nl, halt.


% distribute( N, Requests, PartialSolutionIn, SolutionOut )
distribute(_, [], Solution, Solution ):- !.
distribute( N, [ [P1,P2,P3] | Requests ], [MorningIn,AfternoonIn,EveningIn], Solution):- 
    permutation( [P1,P2,P3], [M,A,E] ), 
    addToListIfNotAlreadyThere(  M,  MorningIn,   Morning  ), 
    addToListIfNotAlreadyThere(  A,  AfternoonIn, Afternoon),
    addToListIfNotAlreadyThere(  E,  EveningIn,   Evening  ),
    length( Morning,   I ),
    length( Afternoon, J ),
    length( Evening,   K ),
    I+J+K =< N, 
    distribute( N, Requests, [Morning, Afternoon , Evening ], Solution ).


addToListIfNotAlreadyThere(X,L,L):- member(X,L),!.
addToListIfNotAlreadyThere(X,L,[X|L]).


